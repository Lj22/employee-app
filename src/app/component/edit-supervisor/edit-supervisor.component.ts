import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Supervisor } from 'src/app/domain/supervisor';
import { ActivatedRoute } from '@angular/router';
import { SupervisorService } from 'src/app/service/supervisor.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-edit-supervisor',
  templateUrl: './edit-supervisor.component.html',
  styleUrls: ['./edit-supervisor.component.css']
})
export class EditSupervisorComponent implements OnInit {
  supervisorForm = new FormGroup({
    sFirstName: new FormControl(''),
    sLastName: new FormControl(''),
    sGender: new FormControl(''),

  });


  constructor(private snackbar:MatSnackBar, private supervisorService:SupervisorService,private route:ActivatedRoute) { }
  sId: number;
  editSup: Supervisor

  ngOnInit() {
    this.sId = +this.route.snapshot.paramMap.get('id');
    
   this.supervisorService.getSupervisorById(+this.sId).subscribe(supervisor=>{
     this.editSup=supervisor
     console.log(this.editSup)
    this.supervisorForm.controls['sFirstName'].setValue(this.editSup.sFirstName);
    this.supervisorForm.controls['sLastName'].setValue(this.editSup.sLastName);
    this.supervisorForm.controls['sGender'].setValue(this.editSup.sGender);
   })
  }

  NeweditSupervisor(){
    this.editSup={
      sId:0,
      sFirstName:this.supervisorForm.controls['sFirstName'].value,
      sLastName:this.supervisorForm.controls['sLastName'].value,
      sGender:this.supervisorForm.controls['sGender'].value,
      }
  
      this.supervisorService.editSupervisor(this.sId, this.editSup)
      .subscribe (supervisor => {
        console.log(supervisor); this.supervisorForm.reset(); this.snackbar.open('Successfully edited supervisor with ID:' +supervisor.sId,'Close',{
          duration:10000,
          verticalPosition:'top'}
        )});
    }
}
