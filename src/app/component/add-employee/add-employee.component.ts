import { Component, OnInit } from '@angular/core';
import { SupervisorService } from 'src/app/service/supervisor.service';
import { Supervisor } from 'src/app/domain/supervisor';
import { FormGroup, FormControl } from '@angular/forms';
import { EmployeeService } from 'src/app/service/employee.service';
import { Employee } from 'src/app/domain/employee';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  employeeForm = new FormGroup({
    lastName: new FormControl(''),
    firstName: new FormControl(''),
    gender: new FormControl(''),
    salary: new FormControl(''),
    resigned: new FormControl(false),
    supervisor_id: new FormControl('')

  });
  constructor(  private snackbar:MatSnackBar,private supervisorService: SupervisorService, private employeeService: EmployeeService) { }
supervisors: Supervisor[];

emp: Employee;

  ngOnInit() {
   this.supervisorService.getSupervisors().subscribe(supervisors=>{
    this.supervisors=supervisors;
   });
  }
  addEmployee(){
    this.emp={id:0,
    lastName:this.employeeForm.controls['lastName'].value,
    firstName:this.employeeForm.controls['firstName'].value,
    gender:this.employeeForm.controls['gender'].value,
    salary:+this.employeeForm.controls['salary'].value,
    resigned:false,
    supervisor:{
      sId:+this.employeeForm.controls['supervisor_id'].value,
      sFirstName:'',
      sLastName:'',
      sGender:''
    }}
    this.employeeService.addEmployee(this.emp)
    .subscribe (employee => {
      console.log(employee); this.employeeForm.reset();
      this.snackbar.open('Successfully added employee with ID:' +employee.id,'Close',{
        duration:10000,
        verticalPosition:'top'}
      )});
  }

}
