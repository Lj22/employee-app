import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddEmployeeComponent } from './component/add-employee/add-employee.component';
import { AddSupervisorComponent } from './component/add-supervisor/add-supervisor.component';
import { ViewEmployeesComponent } from './component/view-employees/view-employees.component';
import { ViewSupervisorsComponent } from './component/view-supervisors/view-supervisors.component';
import { EditSupervisorComponent } from './component/edit-supervisor/edit-supervisor.component';
import { EditEmployeeComponent } from './component/edit-employee/edit-employee.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import { HttpClientModule } from '@angular/common/http';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { HeaderComponent } from './component/header/header.component';
import { EmployeeService } from './service/employee.service';
import { SupervisorService } from './service/supervisor.service';
import {MatMenuModule} from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';
import {MatSortModule} from '@angular/material/sort';
import { EmployeeDetailsComponent } from './component/employee-details/employee-details.component';
import { SupervisorDetailsComponent } from './component/supervisor-details/supervisor-details.component';

@NgModule({
  declarations: [
    AppComponent,
    AddEmployeeComponent,
    AddSupervisorComponent,
    ViewEmployeesComponent,
    ViewSupervisorsComponent,
    EditSupervisorComponent,
    EditEmployeeComponent,
    HeaderComponent,
    EmployeeDetailsComponent,
    SupervisorDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatMenuModule,
    MatSelectModule,
    MatSortModule
  ],
  providers: [EmployeeService,SupervisorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
