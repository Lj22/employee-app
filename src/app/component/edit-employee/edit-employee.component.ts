import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { EmployeeService } from 'src/app/service/employee.service';
import { SupervisorService } from 'src/app/service/supervisor.service';
import { ActivatedRoute } from '@angular/router';
import { Supervisor } from 'src/app/domain/supervisor';
import { Employee } from 'src/app/domain/employee';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {
  id:number;
  employeeForm = new FormGroup({
    lastName: new FormControl(''),
    firstName: new FormControl(''),
    gender: new FormControl(''),
    salary: new FormControl(''),
    resigned: new FormControl(false),
    supervisor_id: new FormControl('')

});
  constructor(  private snackbar:MatSnackBar, private employeeService:EmployeeService,private supervisorService:SupervisorService,private route:ActivatedRoute) { }
  supervisors: Supervisor[] = [];

  editEmp:Employee


  ngOnInit() {
    this.supervisorService.getSupervisors()
  .subscribe(s => {
    this.supervisors = s;
    console.log(this.supervisors)
  });

  this.id = +this.route.snapshot.paramMap.get('id');
  //  alert(this.id)

   this.employeeService.getEmployeeById(this.id).subscribe(e=>{
     console.log(e);
     this.editEmp=e;

     this.employeeForm.controls['firstName'].setValue(this.editEmp.firstName);
      this.employeeForm.controls['lastName'].setValue(this.editEmp.lastName);
      this.employeeForm.controls['gender'].setValue(this.editEmp.gender);
      this.employeeForm.controls['salary'].setValue(this.editEmp.salary);
      this.employeeForm.controls['resigned'].setValue(this.editEmp.resigned);
   })



  
   
 }

 NeweditEmployee(){
  //  alert()
   this.editEmp={
     id:0,
     firstName:this.employeeForm.controls['firstName'].value,
     lastName:this.employeeForm.controls['lastName'].value,
     gender:this.employeeForm.controls['gender'].value,
     salary:+this.employeeForm.controls['salary'].value,
     resigned:this.employeeForm.controls['resigned'].value,
     supervisor:{
       sId:+this.employeeForm.controls['supervisor_id'].value,
       sFirstName:'',
       sLastName:'',
       sGender:''
     }}
 
     this.employeeService.editEmployee(this.id, this.editEmp)
     .subscribe (employee => {
       console.log(employee); this.employeeForm.reset(); 
       this.snackbar.open('Successfully edited employee with ID:' +employee.id,'Close',{
        duration:10000,
        verticalPosition:'top'}
      )});
   } 
 }

  
