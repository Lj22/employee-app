import { Component, OnInit, ViewChild } from '@angular/core';
import { Supervisor } from 'src/app/domain/supervisor';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { SupervisorService } from 'src/app/service/supervisor.service';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-view-supervisors',
  templateUrl: './view-supervisors.component.html',
  styleUrls: ['./view-supervisors.component.css']
})
export class ViewSupervisorsComponent implements OnInit {

  supervisors: Supervisor[]=[];
  dataSource = new MatTableDataSource<Supervisor>();
  displayedColumns=['sId','sFirstName','sLastName','sGender','details','edit','delete'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }



  constructor(private supervisorService:SupervisorService ) { }

  ngOnInit() {
    this.supervisorService.getSupervisors()
    .subscribe(supervisors =>
      {this.supervisors=supervisors;
      this.dataSource.data = this.supervisors;  
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort});
  }
  deleteSupervisor(sId:number){
    this.supervisorService.deleteSupervisor(sId)
    .subscribe(s=> window.location.reload());
  }

}
