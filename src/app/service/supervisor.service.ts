import { Injectable } from '@angular/core';
import { Supervisor } from '../domain/supervisor';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from '../domain/employee';

@Injectable({
  providedIn: 'root'
})
export class SupervisorService {
  private url="http://localhost:5000/supervisor";

  constructor(private http: HttpClient) {}

  getSupervisors(): Observable<Supervisor[]>{
    return this.http.get<Supervisor[]>(this.url);
  }

  addSupervisor(supervisor:Supervisor):
    Observable<Supervisor>{
      return this.http.post<Supervisor>(this.url,supervisor);
    }
    deleteSupervisor(sId:number):Observable<void>{
      return this.http.delete<void>(this.url + '?id=' + sId);
      }  
    editSupervisor(sId: number, supervisor: Supervisor): Observable<Supervisor>{
        return this.http.put<Supervisor>(this.url + '?id=' + sId, supervisor);
      }
    
      getSupervisorById(sId: number): Observable<Supervisor>{
        return this.http.get<Supervisor>(this.url + '/getId?id=' + sId);
      }
     
}
