import { Component, OnInit, ViewChild } from '@angular/core';
import { EmployeeService } from 'src/app/service/employee.service';
import { Employee } from 'src/app/domain/employee';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-view-employees',
  templateUrl: './view-employees.component.html',
  styleUrls: ['./view-employees.component.css']
})
export class ViewEmployeesComponent implements OnInit {
  employees: Employee[]=[];
  dataSource = new MatTableDataSource<Employee>();
  displayedColumns=['id','firstName','lastName','salary','gender','resigned','supervisor','details','edit','delete'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private employeeService:EmployeeService ) { }

  ngOnInit() {
    this.employeeService.getEmployees()
    .subscribe(employees =>
      {(console.log(employees));this.employees=employees;
      this.dataSource.data = this.employees; 
      // alert(this.employees[0].supervisor.sId) 
     
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      console.log(this.paginator)
  
  });
  }

  deleteEmployee(id:number){
    this.employeeService.deleteEmployee(id)
    .subscribe(s=> window.location.reload());
  }

  }


