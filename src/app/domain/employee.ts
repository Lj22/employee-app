import { Supervisor } from './supervisor';

export class Employee {
    public id:number;
    public lastName:string;
    public firstName:string;
    public gender:string;
    public salary:number;
    public resigned:boolean;
    public supervisor:Supervisor;

    
}
