import { Component, OnInit, ViewChild } from '@angular/core';
import { Supervisor } from 'src/app/domain/supervisor';
import { Employee } from 'src/app/domain/employee';
import { EmployeeService } from 'src/app/service/employee.service';
import { SupervisorService } from 'src/app/service/supervisor.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-supervisor-details',
  templateUrl: './supervisor-details.component.html',
  styleUrls: ['./supervisor-details.component.css']
})
export class SupervisorDetailsComponent implements OnInit {

  id: number;
  supervisorDetails: Supervisor;
  employeeList: Employee[];

  dataSource = new MatTableDataSource<Employee>();
  displayedColumns=['id','firstName','lastName','salary','gender','resigned'];

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  constructor(
    private employeeService: EmployeeService,
    private supervisorService: SupervisorService,
    private route: ActivatedRoute, private router: Router)
     { }

  ngOnInit() {
    this.supervisorDetails = new Supervisor();
    this.id = this.route.snapshot.params['id'];
    this.supervisorService.getSupervisorById(this.id)
    .subscribe(data => {
      console.log(data)
      this.supervisorDetails = data;
      this.employeeService.getEmployeesBySId(this.id).subscribe(emp=>{
        this.employeeList=emp;
        this.dataSource.data = this.employeeList;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        console.log(this.dataSource.data);
        });
    }, error => console.log(error));


    
  }
}