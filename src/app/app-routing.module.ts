import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewEmployeesComponent } from './component/view-employees/view-employees.component';
import { EditEmployeeComponent } from './component/edit-employee/edit-employee.component';
import { AddEmployeeComponent } from './component/add-employee/add-employee.component';
import { AddSupervisorComponent } from './component/add-supervisor/add-supervisor.component';
import { EditSupervisorComponent } from './component/edit-supervisor/edit-supervisor.component';
import { ViewSupervisorsComponent } from './component/view-supervisors/view-supervisors.component';
import { EmployeeDetailsComponent } from './component/employee-details/employee-details.component';
import { SupervisorDetailsComponent } from './component/supervisor-details/supervisor-details.component';



const routes: Routes = [
  {path:'view-employee',component: ViewEmployeesComponent},
  {path:'view-supervisor',component: ViewSupervisorsComponent},
  {path:'add-employee',component: AddEmployeeComponent},
  {path:'edit-employee/:id',component: EditEmployeeComponent},
  {path:'add-supervisor',component: AddSupervisorComponent},
  {path:'edit-supervisor/:id',component: EditSupervisorComponent},
  {path:'details-employee/:id',component: EmployeeDetailsComponent},
  {path:'details-supervisor/:id',component: SupervisorDetailsComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
