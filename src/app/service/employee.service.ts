import { Injectable } from '@angular/core';
import { Employee } from '../domain/employee';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private url="http://localhost:5000/employee";

  constructor(private http: HttpClient) {}

  getEmployees(): Observable<Employee[]>{
    return this.http.get<Employee[]>(this.url);
  }

  addEmployee(employee:Employee): Observable<Employee>{
      return this.http.post<Employee>(this.url,employee);
    }
  deleteEmployee(id:number):Observable<number>{
    return this.http.delete<number>(this.url + '?id=' + id);
    }  
    editEmployee(id: number, employee: Employee): Observable<Employee>{
      return this.http.put<Employee>(this.url + '?id=' + id, employee);
    }
  
    getEmployeeById(id: number): Observable<Employee>{ //employee/getId?id=10
      return this.http.get<Employee>(this.url + '/getId?id=' + id);
    }
    getEmployeesBySId(id: number): Observable<Employee[]>{ //localhost:5000/employee/sId?id=7
      return this.http.get<Employee[]>(this.url + '/sId?id=' + id);
    }
  }