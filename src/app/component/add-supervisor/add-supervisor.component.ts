import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Supervisor } from 'src/app/domain/supervisor';
import { SupervisorService } from 'src/app/service/supervisor.service';
import { EmployeeService } from 'src/app/service/employee.service';
import { Employee } from 'src/app/domain/employee';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-supervisor',
  templateUrl: './add-supervisor.component.html',
  styleUrls: ['./add-supervisor.component.css']
})
export class AddSupervisorComponent implements OnInit {
  supervisorForm = new FormGroup({
    sFirstName: new FormControl(''),
    sLastName: new FormControl(''),
    sGender: new FormControl(''),

  });

  constructor( private snackbar:MatSnackBar,private supervisorService: SupervisorService,private employeeService:EmployeeService) { }
  
  sup: Supervisor;

  ngOnInit() {
 
  }

  addSupervisor(){
    this.sup={sId:0,
    sFirstName:this.supervisorForm.controls['sFirstName'].value,
    sLastName:this.supervisorForm.controls['sLastName'].value,
    sGender:this.supervisorForm.controls['sGender'].value,
   
    }
    this.supervisorService.addSupervisor(this.sup)
    .subscribe (supervisor => {
      console.log(supervisor); this.supervisorForm.reset();this.snackbar.open('Successfully added supervisor with ID:' + supervisor.sId,'Close',{
        duration:10000,
        verticalPosition:'top'}
      )});
  }
}
